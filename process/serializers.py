from rest_framework import serializers
import process.models as pmodels
from backend.serializer_fields import PersonKeyField, AMKeyField


class ProcessSerializer(serializers.HyperlinkedModelSerializer):
    frozen_by = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="persons-detail")
    approved_by = serializers.HyperlinkedRelatedField(
        read_only=True, view_name="persons-detail")

    class Meta:
        model = pmodels.Process
        fields = (
            'id', 'person', 'applying_for', 'started',
            'frozen_by', 'frozen_time',
            'approved_by', 'approved_time',
            'closed_by', 'closed_time',
            'fd_comment', 'hide_until',
            'rt_request', 'rt_ticket')

    def __init__(self, *args, **kw):
        super().__init__(*args, **kw)
        import backend.serializers as bserializers

        # See https://github.com/encode/django-rest-framework/issues/2555
        self.fields['person'] = bserializers.PersonSerializer(
            context=self.context)

        # See https://stackoverflow.com/questions/33459501/django-rest-framework-change-serializer-or-add-field-based-on-authentication
        fields = set((
            'id', 'person', 'applying_for', 'started',
            'frozen_by', 'frozen_time',
            'approved_by', 'approved_time',
            'closed_by', 'closed_time',
            'rt_ticket'))

        request = kw["context"].get("request")
        if request is not None and not request.user.is_anonymous:
            if request.user.is_superuser:
                fields.update(('fd_comment', 'rt_request'))

        for field_name in self.fields.keys() - fields:
            self.fields.pop(field_name)


class RequirementTypeField(serializers.Field):
    def to_representation(self, value):
        return value.type

    def to_internal_value(self, data):
        return data


class FingerprintField(serializers.Field):
    def to_representation(self, value):
        return value.fpr

    def to_internal_value(self, data):
        return data


class LogExportSerializer(serializers.ModelSerializer):
    changed_by = PersonKeyField()
    requirement = RequirementTypeField(allow_null=True)

    class Meta:
        model = pmodels.Log
        exclude = ["id", "process"]


class StatementExportSerializer(serializers.ModelSerializer):
    uploaded_by = PersonKeyField()
    fpr = FingerprintField(allow_null=True)

    class Meta:
        model = pmodels.Statement
        exclude = ["id", "requirement"]


class StatementRedactedExportSerializer(serializers.ModelSerializer):
    uploaded_by = PersonKeyField()
    fpr = FingerprintField(allow_null=True)

    class Meta:
        model = pmodels.Statement
        fields = ["fpr", "statement", "uploaded_by", "uploaded_time"]


class AMAssignmentExportSerializer(serializers.ModelSerializer):
    assigned_by = PersonKeyField()
    unassigned_by = PersonKeyField(allow_null=True)
    am = AMKeyField()

    class Meta:
        model = pmodels.AMAssignment
        exclude = ["id", "process"]


class AMAssignmentRedactedExportSerializer(serializers.ModelSerializer):
    assigned_by = PersonKeyField()
    unassigned_by = PersonKeyField(allow_null=True)
    am = AMKeyField()

    class Meta:
        model = pmodels.AMAssignment
        fields = ["am", "paused", "assigned_by", "assigned_time", "unassigned_by", "unassigned_time"]


class RequirementExportSerializer(serializers.ModelSerializer):
    statements = StatementExportSerializer(many=True)
    approved_by = PersonKeyField(allow_null=True)

    class Meta:
        model = pmodels.Requirement
        exclude = ["id", "process"]


class RequirementRedactedExportSerializer(serializers.ModelSerializer):
    statements = StatementRedactedExportSerializer(many=True)
    approved_by = PersonKeyField(allow_null=True)

    class Meta:
        model = pmodels.Requirement
        fields = ["type", "approved_by", "approved_time", "statements"]


class ProcessExportSerializer(serializers.ModelSerializer):
    requirements = RequirementExportSerializer(many=True)
    ams = AMAssignmentExportSerializer(many=True, allow_empty=True)
    log = LogExportSerializer(many=True, allow_empty=True)
    frozen_by = PersonKeyField(allow_null=True)
    approved_by = PersonKeyField(allow_null=True)
    closed_by = PersonKeyField(allow_null=True)

    class Meta:
        model = pmodels.Process
        exclude = ["id", "person"]


class ProcessRedactedExportSerializer(serializers.ModelSerializer):
    requirements = RequirementRedactedExportSerializer(many=True)
    ams = AMAssignmentRedactedExportSerializer(many=True, allow_empty=True)
    frozen_by = PersonKeyField(allow_null=True)
    approved_by = PersonKeyField(allow_null=True)
    closed_by = PersonKeyField(allow_null=True)

    class Meta:
        model = pmodels.Process
        fields = ["applying_for", "started",
                  "frozen_by", "frozen_time",
                  "approved_by", "approved_time",
                  "closed_by", "closed_time",
                  "hide_until", "ams", "requirements"]

    def to_representation(self, obj):
        res = super().to_representation(obj)
        res["fd_comment"] = ""
        res["log"] = []
        return res
