from __future__ import annotations
from django.urls import path
from . import views

urlpatterns = [
    path('', views.List.as_view(), name="process_list"),
    # AM Personal page
    path('am-dashboard/', views.AMDashboard.as_view(), name="process_am_dashboard"),
    path('create/<key>/', views.Create.as_view(), name="process_create"),
    path('<int:pk>/', views.Show.as_view(), name="process_show"),
    path('<int:pk>/intent/', views.ReqIntent.as_view(), name="process_req_intent"),
    path('<int:pk>/sc_dmup/', views.ReqAgreements.as_view(), name="process_req_sc_dmup"),
    path('<int:pk>/advocate/', views.ReqAdvocate.as_view(), name="process_req_advocate"),
    path('<int:pk>/am_ok/', views.ReqAM.as_view(), name="process_req_am_ok"),
    path('<int:pk>/keycheck/', views.ReqKeycheck.as_view(), name="process_req_keycheck"), # TODO: test
    path('<int:pk>/approval/', views.ReqApproval.as_view(), name="process_req_approval"), # TODO: test
    path('<int:pk>/add_log/', views.AddProcessLog.as_view(), name="process_add_log"),
    path('<int:pk>/<type>/statement/create/', views.StatementCreate.as_view(), name="process_statement_create"),
    path('<int:pk>/<type>/statement/<int:st>/delete/', views.StatementDelete.as_view(), name="process_statement_delete"),
    path('<int:pk>/<type>/statement/<int:st>/raw/', views.StatementRaw.as_view(), name="process_statement_raw"),
    path('<int:pk>/assign_am/', views.AssignAM.as_view(), name="process_assign_am"),
    path('<int:pk>/unassign_am/', views.UnassignAM.as_view(), name="process_unassign_am"),
    path('<int:pk>/mailbox/download/', views.MailArchive.as_view(), name="process_mailbox_download"), # TODO: test
    path('<int:pk>/mailbox/', views.DisplayMailArchive.as_view(), name="process_mailbox_show"), # TODO: test
    path('<int:pk>/update_keycheck/', views.UpdateKeycheck.as_view(), name="process_update_keycheck"), # TODO: test
    path('<int:pk>/download_statements/', views.DownloadStatements.as_view(), name="process_download_statements"), # TODO: test
    path('emeritus/', views.Emeritus.as_view(), name="process_emeritus_self"),
    path('emeritus/<key>/', views.Emeritus.as_view(), name="process_emeritus"),
    path('<int:pk>/cancel/', views.Cancel.as_view(), name="process_cancel"),
]
