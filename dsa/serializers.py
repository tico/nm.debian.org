from rest_framework import serializers
import dsa.models as dmodels
from backend.serializer_fields import PersonKeyField


class LDAPFieldsSerializer(serializers.HyperlinkedModelSerializer):
    person = PersonKeyField(allow_null=True)

    class Meta:
        model = dmodels.LDAPFields
        fields = (
            'person',
            'cn', 'mn', 'sn',
            'email',
            'uid',
        )

    def to_representation(self, instance):
        res = super().to_representation(instance)

        # Remove fields that the given user is not allowed to see
        allowed_fields = {'person', 'cn', 'mn', 'sn', 'uid'}

        request = self.context.get("request")
        if request is not None and not request.user.is_anonymous:
            if request.user.is_superuser:
                allowed_fields.add('email')

        for field_name in res.keys() - allowed_fields:
            res.pop(field_name)

        return res
