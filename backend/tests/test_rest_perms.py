from django.test import TestCase
from django.urls import reverse
from backend.unittest import PersonFixtureMixin


class PersonFieldsMixin:
    PERSON_ANON_FIELDS = ('id', 'fullname', 'bio', 'status', 'status_changed', 'fpr')
    PERSON_DD_FIELDS = ('is_staff', 'email')
    PERSON_ADMIN_FIELDS = ('fd_comment',)
    LDAPFIELDS_ANON_FIELDS = ('cn', 'mn', 'sn', 'uid')
    LDAPFIELDS_DD_FIELDS = ()
    LDAPFIELDS_ADMIN_FIELDS = ('email',)

    def assertAllPersonFieldsAccountedFor(self, record):
        all_fields = frozenset(self.PERSON_ANON_FIELDS + self.PERSON_DD_FIELDS + self.PERSON_ADMIN_FIELDS + ('ldap_fields',))
        self.assertFalse(record.keys() - all_fields)

    def assertPersonAnonFields(self, record):
        ldap_fields = record.get("ldap_fields")
        for field in self.PERSON_ANON_FIELDS:
            self.assertIn(field, record)
        for field in self.PERSON_DD_FIELDS + self.PERSON_ADMIN_FIELDS:
            self.assertNotIn(field, record)

        for field in self.LDAPFIELDS_ANON_FIELDS:
            self.assertIn(field, ldap_fields)
        for field in self.LDAPFIELDS_DD_FIELDS + self.LDAPFIELDS_ADMIN_FIELDS:
            self.assertNotIn(field, ldap_fields)

        self.assertAllPersonFieldsAccountedFor(record)

    def assertPersonDDFields(self, record):
        ldap_fields = record.get("ldap_fields")
        for field in self.PERSON_ANON_FIELDS + self.PERSON_DD_FIELDS:
            self.assertIn(field, record)
        for field in self.PERSON_ADMIN_FIELDS:
            self.assertNotIn(field, record)

        for field in self.LDAPFIELDS_ANON_FIELDS + self.LDAPFIELDS_DD_FIELDS:
            self.assertIn(field, ldap_fields)
        for field in self.LDAPFIELDS_ADMIN_FIELDS:
            self.assertNotIn(field, ldap_fields)

        self.assertAllPersonFieldsAccountedFor(record)

    def assertPersonAdminFields(self, record):
        ldap_fields = record.get("ldap_fields")
        for field in self.PERSON_ANON_FIELDS + self.PERSON_DD_FIELDS + self.PERSON_ADMIN_FIELDS:
            self.assertIn(field, record)
        for field in self.LDAPFIELDS_ANON_FIELDS + self.LDAPFIELDS_DD_FIELDS + self.LDAPFIELDS_ADMIN_FIELDS:
            self.assertIn(field, ldap_fields)
        self.assertAllPersonFieldsAccountedFor(record)


class RestPersonTestCase(PersonFixtureMixin, PersonFieldsMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for visitor in "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r":
            cls._add_method(cls._test_anon, visitor)

        for visitor in "dd_nu", "dd_u":
            cls._add_method(cls._test_dd, visitor)

        for visitor in "fd", "dam":
            cls._add_method(cls._test_admin, visitor)

    def _test_anon(self, visitor):
        client = self.make_test_apiclient(visitor)
        response = client.get(reverse('persons-list'), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 13)
        self.assertPersonAnonFields(response.data[0])

        response = client.get(reverse('persons-detail', args=[self.persons.fd.pk]), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertPersonAnonFields(response.data)

    def _test_dd(self, visitor):
        client = self.make_test_apiclient(visitor)
        response = client.get(reverse('persons-list'), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 13)
        self.assertPersonDDFields(response.data[0])

        response = client.get(reverse('persons-detail', args=[self.persons.fd.pk]), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertPersonDDFields(response.data)

    def _test_admin(self, visitor):
        client = self.make_test_apiclient(visitor)
        response = client.get(reverse('persons-list'), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 13)
        self.assertPersonAdminFields(response.data[0])

        response = client.get(reverse('persons-detail', args=[self.persons.fd.pk]), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertPersonAdminFields(response.data)
