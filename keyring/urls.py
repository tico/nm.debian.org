from django.urls import re_path
from . import views


urlpatterns = [
    re_path(r'^keycheck/(?P<fpr>[0-9A-Fa-f]{32,40})$', views.Keycheck.as_view(), name="keyring_keycheck"),
]
