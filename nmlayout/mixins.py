from __future__ import annotations
from typing import Optional
from dataclasses import dataclass
from nm2.lib import assets


@dataclass
class NavLink:
    url: str
    label: str
    icon: Optional[str] = None
    disabled: Optional[bool] = False  # if True, link will be grayed out
    target_blank: Optional[bool] = False  # if true, opens link in a new tab


class NM2LayoutMixin(assets.AssetMixin):
    assets = [assets.Bootstrap4, assets.ForkAwesome]

    def get_person_menu_entries(self):
        return []

    def get_process_menu_entries(self):
        return []

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["navbar"] = {
            "person": self.get_person_menu_entries(),
            "process": self.get_process_menu_entries(),
        }
        return ctx


class SignonMixin(NM2LayoutMixin):
    """
    Mixin for signon views
    """
    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["base_template"] = "nm2-base.html"
        return ctx
