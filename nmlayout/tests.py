from django.test import TestCase
from django.urls import reverse
from backend.unittest import BaseFixtureMixin
from backend import const


class TestStatusAllPermissions(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        cls.create_person("dd_nu", status=const.STATUS_DD_NU)
        fd = cls.create_person("fd", status=const.STATUS_DD_NU)
        cls.ams.create("fd", person=fd, is_fd=True)

    def test_anonymous(self):
        client = self.make_test_client(None)
        self.assertGetSuccess(client)

    def test_dc(self):
        client = self.make_test_client("dc")
        self.assertGetSuccess(client)

    def test_dd(self):
        client = self.make_test_client("dd_nu")
        self.assertGetSuccess(client)

    def test_fd(self):
        client = self.make_test_client("fd")
        self.assertGetSuccess(client)

    def assertGetSuccess(self, client):
        response = client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
