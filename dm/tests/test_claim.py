"""
Test DM claim interface
"""
from __future__ import annotations
from typing import Tuple
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.models import Person, Fingerprint
from backend.unittest import PersonFixtureMixin
from signon.models import Identity
from signon import providers


@override_settings(SIGNON_PROVIDERS=[
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
    providers.BaseSessionProvider(name="salsa", label="Salsa", single_bind=True),
])
class TestClaim(PersonFixtureMixin, TestCase):
    # Use an old, not yet revoked key of mine
    test_fingerprint = "66B4DFB68CB24EBBD8650BC4F4B4B0CC797EBFAB"

    @classmethod
    def __add_extra_tests__(cls):
        for person in ("pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r"):
            cls._add_method(cls._test_success, person, "debsso")
            cls._add_method(cls._test_success, person, "salsa")

        for person in ("dd_nu", "dd_u", "fd", "dam"):
            cls._add_method(cls._test_is_dd, person, "debsso")
            cls._add_method(cls._test_is_dd, person, "salsa")

    def get_identity_for_issuer(self, person, issuer):
        person = self.persons[person]
        if issuer == "debsso":
            return self.identities.create(
                    "debsso", issuer="debsso",
                    subject=f"{person.ldap_fields.uid}@users.alioth.debian.org",
                    username=f"{person.ldap_fields.uid}@users.alioth.debian.org", audit_skip=True)
        elif issuer == "salsa":
            return self.identities.create(
                    "salsa", issuer="salsa",
                    subject=f"{person.pk}", audit_skip=True)
        else:
            raise NotImplementedError("Issuer {issuer} not supported in test")

    def get_confirm_url(self, person, issuer) -> Tuple[str, str]:
        """
        Set up a test case where person has an invalid username and
        self.test_fingerprint as fingerprint, and request a claim url.

        Returns the original username of the person, and the plaintext claim
        url.
        """
        # Get an unbound identity
        identity = self.get_identity_for_issuer(person, issuer)
        self.assertIsNone(identity.person)

        # Make sure the person has a key associated
        person = self.persons[person]
        Fingerprint.objects.create(fpr=self.test_fingerprint, person=person, is_active=True, audit_skip=True)

        # Visit as active but unbound identity
        client = self.make_test_client(None, [identity])
        response = client.get(reverse("dm_claim"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["identity"], identity)
        response = client.post(reverse("dm_claim"), data={"fpr": self.test_fingerprint})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["identity"], identity)
        self.assertEqual(response.context["fpr"].fpr, self.test_fingerprint)

        # Get and return the confirmation url from the result
        self.assertIn("/dm/claim/confirm", response.context["plaintext"])
        self.assertIn("-----BEGIN PGP MESSAGE-----", response.context["challenge"])
        return identity, response.context["plaintext"].strip()

    def _test_success(self, person, issuer):
        identity, confirm_url = self.get_confirm_url(person, issuer)
        client = self.make_test_client(None, [identity])
        response = client.get(confirm_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["errors"], [])
        # The identity is now bound
        identity.refresh_from_db()
        self.assertEqual(identity.person, self.persons[person])

    def _test_is_dd(self, person, issuer):
        identity, confirm_url = self.get_confirm_url(person, issuer)
        client = self.make_test_client(None, [identity])
        response = client.get(confirm_url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context["errors"], ["The GPG fingerprint corresponds to a Debian Developer."])
        person = Person.objects.get(pk=self.persons[person].pk)
        # The identity is still unbound
        identity.refresh_from_db()
        self.assertIsNone(identity.person)

    def test_anonymous(self):
        client = self.make_test_client(None)
        response = client.get(reverse("dm_claim"))
        self.assertPermissionDenied(response)
        response = client.get(reverse("dm_claim_confirm", kwargs={"token": "123456"}))
        self.assertPermissionDenied(response)
