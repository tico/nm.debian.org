from rest_framework import serializers
from backend.serializer_fields import PersonKeyField
from . import models


class AuditLogExportSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AuditLog
        exclude = ["id", "key"]


class KeyExportSerializer(serializers.ModelSerializer):
    user = PersonKeyField()
    audit_log = AuditLogExportSerializer(many=True)

    class Meta:
        model = models.Key
        exclude = ["id"]
