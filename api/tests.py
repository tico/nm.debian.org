from django.test import TestCase
from django.urls import reverse
from backend.unittest import BaseFixtureMixin, NamedObjects
from backend import const
from apikeys.models import Key


class TestStatusAllPermissions(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(keys=NamedObjects(Key))

        p = cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        username = "dc@users.alioth.debian.org"
        cls.identities.create(
                "dc_debsso", person=p, issuer="debsso", subject=username, username=username, audit_skip=True)
        cls.keys.create("dc", user=p, name="test", value="testkey", enabled=True)

        p = cls.create_person("dd_nu", status=const.STATUS_DD_NU)
        username = "dd_nu@debian.org"
        cls.identities.create(
                "dd_nu_debsso", person=p, issuer="debsso", subject=username, username=username, audit_skip=True)

    def test_anonymous(self):
        client = self.make_test_client(None)
        self.assertGetAllForbidden(client)
        self.assertGetByStatusForbidden(client)
        self.assertGetNamedAllowed(client)

    def test_loggedin(self):
        client = self.make_test_client("dc")
        self.assertGetAllAllowed(client)
        self.assertGetByStatusAllowed(client)
        self.assertGetNamedAllowed(client)

    def test_apitoken(self):
        client = self.make_test_client(None)
        client.defaults["HTTP_API_KEY"] = self.keys.dc.value
        self.assertGetAllAllowed(client)
        self.assertGetByStatusAllowed(client)
        self.assertGetNamedAllowed(client)

    def assertGetAllAllowed(self, client):
        response = client.get(reverse("api:status"))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {
                'dc@users.alioth.debian.org': {'status': 'dc'},
                'dd_nu@debian.org': {'status': 'dd_nu'},
            }
        })

    def assertGetAllForbidden(self, client):
        response = client.get(reverse("api:status"))
        self.assertPermissionDenied(response)

    def assertGetByStatusAllowed(self, client):
        response = client.get(reverse("api:status"), data={"status": "dd_u,dd_nu"})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {'dd_nu@debian.org': {'status': 'dd_nu'}},
        })

    def assertGetByStatusForbidden(self, client):
        response = client.get(reverse("api:status"), data={"status": "dd_u,dd_nu"})
        self.assertPermissionDenied(response)

    def assertGetNamedAllowed(self, client):
        response = client.get(reverse("api:status"), data={"person": "dd_nu@debian.org"})
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {'dd_nu@debian.org': {'status': 'dd_nu'}},
        })

        response = client.post(
                reverse("api:status"),
                data='["dd_nu@debian.org","dc@users.alioth.debian.org"]',
                content_type="application/json")
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {
            'people': {
                'dc@users.alioth.debian.org': {'status': 'dc'},
                'dd_nu@debian.org': {'status': 'dd_nu'}
            },
        })


class TestSalsaStatus(BaseFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.add_named_objects(keys=NamedObjects(Key))

        cls.create_person("dc", status=const.STATUS_DC, alioth=True)
        cls.create_person("dd_nu", status=const.STATUS_DD_NU)

        cls.identities.create(
                "dc", person=cls.persons.dc, issuer="salsa", subject="1", username="dc", audit_skip=True)
        cls.identities.create(
                "dd_nu", person=cls.persons.dd_nu, issuer="salsa", subject="2", username="dd_nu", audit_skip=True)
        cls.identities.create(
                "unmapped", issuer="salsa", subject="3", username="unmapped", audit_skip=True)

    def test_missing(self):
        client = self.make_test_client(None)
        response = client.get(reverse("api:salsa_status", kwargs={"subject": 4}))
        self.assertEqual(response.status_code, 404)

    def test_unmapped(self):
        client = self.make_test_client(None)
        response = client.get(reverse("api:salsa_status", kwargs={"subject": 3}))
        self.assertJSONEqual(response.content, {
            "profile": None,
            "status": None,
            "uid": None,
        })

    def test_existing(self):
        client = self.make_test_client(None)
        response = client.get(reverse("api:salsa_status", kwargs={"subject": 1}))
        self.assertJSONEqual(response.content, {
            "profile": "/person/dc/",
            "status": "dc",
            "uid": "dc",
        })
        response = client.get(reverse("api:salsa_status", kwargs={"subject": 2}))
        self.assertJSONEqual(response.content, {
            "profile": "/person/dd_nu/",
            "status": "dd_nu",
            "uid": "dd_nu",
        })
