from __future__ import annotations
from django.urls import path
from . import views

app_name = "impersonate"

urlpatterns = [
    # Impersonate a user
    path('impersonate/', views.Impersonate.as_view(), name="impersonate"),
    path('whoami/', views.Whoami.as_view(), name="whoami"),
]
