from __future__ import annotations
from django.test import TestCase, override_settings
from django.urls import reverse
from backend.unittest import PersonFixtureMixin
import minechangelogs.models as mmodels
import tempfile
import os


class TestEntry:
    def __init__(self, pkgname="test", version="0.1", date="", maint="test@debian.org", text=""):
        self.pkgname = pkgname
        self.version = version
        self.date = date
        self.maint = maint
        self.text = text

    def make_indexing_entry(self):
        body = """{self.pkgname} ({self.version}) unstable; urgency=medium

      * {self.text}
""".format(self=self)
        return ("{self.pkgname}_{self.version}".format(self=self), self.date, self.maint, body)

    def make_full_entry(self):
        return """{self.pkgname} ({self.version}) unstable; urgency=medium

      * {self.text}

 -- {self.maint}  {self.date}""".format(self=self)


class TestIndexMixin:
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.url = reverse("minechangelogs:search", kwargs={"key": cls.persons.dc.lookup_key})
        cls.tmpdir = tempfile.TemporaryDirectory()
        cls.entry1 = TestEntry(version="0.1", date="Sun, 11 Dec 2016 08:15:50", text="thanks test for #123")
        cls.entry2 = TestEntry(version="0.2", date="Sun, 12 Dec 2016 08:15:50", text="test test for #124")
        cls.entry3 = TestEntry(version="0.3", date="Sun, 13 Dec 2016 08:15:50",
                               text="reproduced issue #780397, thanks Jean-Michel Nirgal Vourgère")
        with cls.test_settings():
            indexer = mmodels.Indexer()
            indexer.index([x.make_indexing_entry() for x in (cls.entry1, cls.entry2, cls.entry3)])
            indexer.flush()

    @classmethod
    def tearDownClass(cls):
        cls.tmpdir.cleanup()
        super().tearDownClass()

    @classmethod
    def test_settings(cls):
        return override_settings(MINECHANGELOGS_INDEXDIR=os.path.join(cls.tmpdir.name, "index"),
                                 MINECHANGELOGS_CACHEDIR=os.path.join(cls.tmpdir.name, "cache"))


class TestMinechangelogs(TestIndexMixin, PersonFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        cls._add_method(cls._test_forbidden, None)
        for visitor in "pending", "dc", "dc_ga", "dm", "dm_ga", "dd_e", "dd_r",  "dd_nu", "dd_u", "fd", "dam":
            cls._add_method(cls._test_success, visitor)

    def _test_success(self, visitor):
        with self.test_settings():
            client = self.make_test_client(self.persons[visitor])
            response = client.get(self.url)
            self.assertEqual(response.status_code, 200)

            response = client.post(self.url, data={"query": "test"})
            self.assertEqual(response.status_code, 200)

            response = client.post(self.url, data={"query": "test", "download": True})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response["Content-Type"], "text/plain")

    def _test_forbidden(self, visitor):
        with self.test_settings():
            client = self.make_test_client(self.persons[visitor])
            response = client.get(self.url)
            self.assertPermissionDenied(response)

    def test_utf8(self):
        # See https://bugs.debian.org/780397
        with self.test_settings():
            client = self.make_test_client(self.persons.dc)
            response = client.post(self.url, data={"query": "Vourgère", "download": True})
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.content.decode("utf-8").rstrip(), self.entry3.make_full_entry())
