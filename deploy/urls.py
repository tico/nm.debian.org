from django.urls import path
from . import views

urlpatterns = [
    path('gitlab-pipeline-hook', views.GitlabPipeline.as_view(), name="deploy_gitlab_pipeline_hook"),
]
